# Versions

| Branch |  Version / Tag | Compatible Ibexa Major Version | PHP Version |
| ------ | ------ | ------ | ------ |
| master | ^4.0.0 | ^4.0 | 7.4 / composer 2 + composer 1 via COMPOSER_VERSION=1 |
| 3.0 | ^3.3.0 | ^3.3 | 7.4 / composer 2 |
| 3.0 | ^3.2.0 | ^3.2.0 | 7.4 / composer 1 |
| 2.5 | ^2.5 | ^2.0 | 7.2 / composer 1 |
| 5.4-legacy | ^1.0 | Legacy ^5.0 && ^4.0 | 7.2 / composer 1 |

# Patch Management

Patch format: Unified diff (diff -u)

Patch categories:

| Prefix | Type |
| ------ | ------ | 
| 1xx | Ibexa Support (Bugs, Features, Tickets) |
| 2xx | Patches part of known future versions of eZ Publish Folder. Those patches are inside a subfolder named after the ez platform version number. |
| 3xx | Ibexa Partner patches (Patches created by XROW GmbH) |
| 4xx | Client Patches ( created by the Client ) |
| 5xx | Other Patches |

Patch naming: 
Category number, patch number, ticket number or/and short description.

e.g.: 202_EZ20005_batch-move-v4.diff

Patches which are part of future versions of eZ Publish should be stored in 
directories named by the version number, eg. 16.04

The Ticketnumber is preceding an prefix for the Ticketsystem were the issue is documented. The prefix can be XROW, EZ, EZSUPPORT or something else.
