FROM registry.gitlab.com/xrow-public/docker-php-centos7:7.4.19
# Versions >=7.4.6 will have composer 2 + Fedora 34
# Versions >=7.4.0 will have composer 2 + EL8
# Versions >7.3.18 will have composer 2 + EL7
# Versions <=7.3.18 will have composer 1 + EL7
ENV COMPOSER_VERSION=

USER 0

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

# Copy extra files to the image.
COPY ./root/ /

RUN chmod 755 /usr/libexec/s2i/assemble && \
    chmod 755 /usr/libexec/s2i/run && \
    rm -Rf /tmp/sessions/ && \
    rm -Rf /tmp/* && \
    /usr/libexec/container-setup && \
    rpm-file-permissions && \
    chmod 600 /etc/ssh/ssh_config.d/50-redhat.conf

USER 1001
