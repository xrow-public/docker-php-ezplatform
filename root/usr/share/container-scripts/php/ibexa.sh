function composer_install() {
  if [ ! -z "$COMPOSER_VERSION" ]; then
    echo "Found COMPOSER_VERSION, installing dependencies using composer.phar... "

    # Install Composer
    TEMPFILE=$(mktemp)
    RETRIES=6
    for ((i=0; i<$RETRIES; i++)); do

      if [ -z "$COMPOSER_INSTALLER" ]; then
        export COMPOSER_INSTALLER="https://getcomposer.org/installer"
      fi

      echo "Downloading $COMPOSER_INSTALLER, attempt $((i+1))/$RETRIES"
      curl -o $TEMPFILE $COMPOSER_INSTALLER && break
      sleep 10
    done
    if [[ $i == $RETRIES ]]; then
      echo "Download failed, giving up."
      exit 1
    fi

    if [ -z $COMPOSER_VERSION ]; then
      echo "By default, latest composer will be used. If you want to use v1 please use the environment variable COMPOSER_VERSION=1"
      php <$TEMPFILE
    else
      echo "You set the COMPOSER_VERSION"
      php <$TEMPFILE -- --$COMPOSER_VERSION
    fi

    if [ "$(ls -a /tmp/artifacts/ 2>/dev/null)" ]; then
      echo "Restoring build artifacts"
      mv /tmp/artifacts/* $HOME/
    fi

    export COMPOSER_EXECUTABLE=./composer.phar
  else
    export COMPOSER_EXECUTABLE=/usr/bin/composer
  fi
}
function ssh_run() {
  set -eo pipefail
  echo "$(date +[%T];) Next. SSH."
  if [ -z "$GIT_SSH_KEY" ]; then
    echo -e "GIT_SSH_KEY is not found for git access.\n"
    return 0
  fi
  echo "$GIT_SSH_KEY" | base64 -d >> /dev/null && GIT_SSH_KEY=$( echo "$GIT_SSH_KEY" | base64 -d ) || echo "Key is not base64 encoded. Please convert for easier handling."
  eval $(ssh-agent -s)
  echo "Adding GIT_SSH_KEY to agent..."
  echo "$GIT_SSH_KEY" | ssh-add -
  ssh-add -l
  mkdir -p ~/.ssh
  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  

  mkdir -p ${HOME}/.ssh
  chmod 700 ${HOME}/.ssh
  touch ${HOME}/.ssh/known_hosts
  chmod 600 ${HOME}/.ssh/known_hosts
  ssh-keyscan -t rsa gitlab.com >> ${HOME}/.ssh/known_hosts
  ssh-keyscan -t rsa github.com >> ${HOME}/.ssh/known_hosts
  echo "$(date +[%T];) Done. SSH."
}

function ssh_stop() {
  set -eo pipefail
  if [ "${SSH_AGENT_PID+1}" = "1" ]; then
      ssh-add -D
      ssh-agent -k > /dev/null 2>&1
      unset SSH_AGENT_PID
      unset SSH_AUTH_SOCK
  fi
}

function patch_run() {
  set -eo pipefail
  echo "$(date +[%T];) Next. Patch."
  dirs="vendor src"
  if [ -d patches ]; then
    echo "Warning: Deprecated way of patching! Use Bundles."
    dirs="$dirs patches"
  fi
  tmpdir=$(mktemp -d -p "./")
  echo "Copy patches found in bundles Resources/patches, patches or src to $tmpdir"
  for file in $(find $dirs -regextype posix-extended  -iregex '((.*/)?Resources/patches/|patches/).*\.(diff|patch)$' | sort); do
    if [ -e "$file" ] ; then
        echo "Found patchfile $file"
        cp $file $tmpdir/
    fi
  done
  echo "Applying patches found in directory $tmpdir"
  for file in $(find $tmpdir -regextype posix-extended -iregex '.*\.(diff|patch)$' | sort); do
    if [ -e "$file" ] ; then
        echo "Applying patch $file"
        patch -p0 --batch --ignore-whitespace -N < $file
    fi
  done
  rm -Rf $tmpdir
  echo "$(date +[%T];) Done. Patch."
}

function yarn_run() {
  set -eo pipefail
  echo "$(date +[%T];) Next. Yarn."

  if [ ! -z "$BUILD_CACHE_DIR" ]; then
    echo "-> Set cache dir to $BUILD_CACHE_DIR..."
    # Must exist on Host System and must be 777
    export NPM_CONFIG_PREFIX=${BUILD_CACHE_DIR}/.npm-global
    export YARN_CACHE_FOLDER=${BUILD_CACHE_DIR}/.yarn
    mkdir -p $NPM_CONFIG_PREFIX || true
    mkdir -p $YARN_CACHE_FOLDER || true 
  fi

  echo "Cache dir: $(yarn cache dir)"
  if [ -f package.json ]; then
    echo "Install webpack and dependencies"
    yarn install
    echo "Compile webpack assets"
    mkdir -p web/assets/build
    mkdir -p web/assets/ezplatform/build
    yarn encore prod
  fi
  echo "$(date +[%T];) Done. Yarn."
}
function composer_run() {
  set -eo pipefail
  echo "$(date +[%T];) Next. Composer."

  if [ ! -f composer.json ]; then
    echo -e "Can't find 'composer.json'\n"
    exit 1
  fi

  if [ ! -z "$BUILD_CACHE_DIR" ]; then
    echo "-> Set cache dir to $BUILD_CACHE_DIR..."
    # Must exist on Host System and must be 777
    export COMPOSER_CACHE_DIR=${BUILD_CACHE_DIR}/.composer
    mkdir -p $COMPOSER_CACHE_DIR || true 
  fi

  echo "Found 'composer.json', installing dependencies using composer.phar... "

  # Change the repo mirror if provided
  if [ -n "$COMPOSER_MIRROR" ]; then
    echo "$(date +[%T];) Composer: COMPOSER_MIRROR $COMPOSER_MIRROR"
    $COMPOSER_EXECUTABLE config repositories.packagist composer $COMPOSER_MIRROR
  fi
  if [ -n "$INSTALLATION_ID" ]; then
    echo "$(date +[%T];) Composer: InstallationID $INSTALLATION_ID"
    $COMPOSER_EXECUTABLE config http-basic.updates.ibexa.co $INSTALLATION_ID $LICENCE_KEY
    $COMPOSER_EXECUTABLE config http-basic.updates.ez.no $INSTALLATION_ID $LICENCE_KEY
  fi
  if [ -n "$GITHUB_KEY" ]; then
    echo "$(date +[%T];) Composer: GITHUB_KEY"
    $COMPOSER_EXECUTABLE config github-oauth.github.com $GITHUB_KEY
  fi
  if [ -n "$GITLAB_TOKEN" ]; then
    echo "$(date +[%T];) Composer: GITLAB_TOKEN"
    $COMPOSER_EXECUTABLE config gitlab-token.gitlab.com $GITLAB_TOKEN
  fi
  if [ -n "$AUTH_JSON" ]; then
    echo "$(date +[%T];) Composer: AUTH_JSON"
    echo $AUTH_JSON > auth.json
  fi

  $COMPOSER_EXECUTABLE config --list

  export COMPOSER_MEMORY_LIMIT=-1
  export COMPOSER_PROCESS_TIMEOUT=1800
  # Install App dependencies using Composer
  command $COMPOSER_EXECUTABLE install --no-interaction --no-ansi --optimize-autoloader --profile  $COMPOSER_ARGS
  
  if [ ! -f composer.lock ]; then
    echo -e "\nConsider adding a 'composer.lock' file into your source repository.\n"
  fi
  echo "$(date +[%T];) Done. Composer."
}
function ibexa_optmize() {
  chmod +x bin/console || true
  mkdir -p ./var/xdebug/
}
function ezplatform_clean() {
  echo "$(date +[%T];) Next. Cleanup."
  rm -Rf ./.cache/
  rm -Rf ./.composer/cache/
  rm -Rf ./.ssh/
  rm -Rf ./.yarn/
  rm -Rf ./.npm-global/
  rm -Rf ./node_modules/
  rm -Rf ./.node-gyp/
  rm -Rf ./var/*/*
  echo "$(date +[%T];) Next. Cleanup Auth."
  if [ -d .git ]; then find .git/ -name 'config' -exec sed -i -E 's/url = http.*\@gitlab.com\//url = git\@gitlab.com:/g' {} \;; fi;
  echo "$(date +[%T];) Done. Cleanup."
}
